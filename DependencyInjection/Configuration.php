<?php

namespace PredanieChatMonologBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('predanie_bitrix24_monolog');

        $rootNode
            ->children()
                ->scalarNode('telegram_chat_id')
                    ->info('Telegram chart id for error reporting')
                    ->defaultValue('1')
                ->end()
                ->scalarNode('telegram_bot_token')
                    ->info('Telegram reporters user id')
                    ->defaultValue('1')
                ->end()
            ->end();

        return $treeBuilder;
    }
}
